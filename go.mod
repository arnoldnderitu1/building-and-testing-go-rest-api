module gitlab.com/arnoldnderitu1/building-and-testing-go-rest-api.git

go 1.13

require (
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.0
)
