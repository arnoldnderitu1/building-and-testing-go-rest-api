package main

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

// a struct that will hold the application
type App struct {
	Router *mux.Router
	DB     *sql.DB
}

// App struct exposes references to the router and the DB
// being used by the app
// `Initialize` takes details required to connect to the DB
// and creates a DB connection then wires up the routes
// `Run` start the application
func (a *App) Initialize(user, password, dbname string) {
	connectionString := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", user, password, dbname)

	var err error
	a.DB, err = sql.Open("postgres", connectionString)
	if err != nil {
		log.Fatal(err)
	}
	a.Router = mux.NewRouter()
}
func (a *App) Run(addr string) {}
